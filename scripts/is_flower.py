from __future__ import division, print_function, absolute_import

import tflearn
from tflearn.layers.core import input_data, dropout, fully_connected
from tflearn.layers.conv import conv_2d, max_pool_2d
from tflearn.layers.estimator import regression
from tflearn.data_preprocessing import ImagePreprocessing
from tflearn.data_augmentation import ImageAugmentation
import time
import scipy
import numpy as np
import os
import argparse
from networkfactory import NetworkFactory

def test_dir():
    parser = argparse.ArgumentParser(description='Decide if an image is a picture of a flower')
    parser.add_argument('--image_dir', type=str, help='The image image file to check', required=True)
    parser.add_argument('--model', type=str, help='The directory of the model to verify against', required=True)
    parser.add_argument('--x', type=int, help='pixel count in x dimension used in model', required=True)
    parser.add_argument('--y', type=int, help='pixel count in y dimension used in model', required=True)
    parser.add_argument('--out_file', type=str, help='The file to write results too', required=True)
    args = parser.parse_args()

    dimension_x = args.x
    dimension_y = args.y

    # Same network definition as before
    img_prep = ImagePreprocessing()
    img_prep.add_featurewise_zero_center()
    img_prep.add_featurewise_stdnorm()

    img_aug = ImageAugmentation()
    img_aug.add_random_flip_leftright()
    img_aug.add_random_rotation(max_angle=25.)
    img_aug.add_random_blur(sigma_max=3.)

    fact = NetworkFactory()

    network = fact.frcnn(dimension_x, dimension_y, img_prep, img_aug)
#    network = fact.conv_2d_2(dimension_x, dimension_y, img_prep, img_aug)

    model = tflearn.DNN(network, checkpoint_path=args.model+'flower-classifier.tfl.ckpt')

    model.load('flower-classifier.tfl')


    for file in os.listdir(args.image_dir):
        if file.endswith('.png'):
            is_flower(dimension_x, dimension_y, file, args.image_dir, args.model, args.out_file, model)

def is_flower(x, y, image, image_dir, testing_model, out, model):
    # Set up args
    #parser.add_argument('--c', type=str, help='classifier to use to test image', required=True)
#    args = parser.parse_args()


    # Load the image file
    img = image_dir+image
    img = scipy.ndimage.imread(img, mode="RGB")

    # Scale image to required dimensions
    img = scipy.misc.imresize(img, (x, y), interp="bicubic").astype(np.float32, casting='unsafe')

    # Predict
    prediction = model.predict([img])

    # Check the result.
    is_flower = np.argmax(prediction[0]) == 1

    with open(out, "a") as f:

        # Print the result
        if is_flower:
            print("That's a flower!")
            f.write("Image: {} IS A FLOWER\n".format(image))
        else:
            print("NOT!")
            f.write("Image: {} is not a flower!\n".format(image))

    f.close()

if __name__ == '__main__':
    test_dir()
