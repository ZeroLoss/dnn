import argparse
import tflearn
from tflearn.data_utils import shuffle
from tflearn.layers.core import input_data, dropout, fully_connected
from tflearn.layers.conv import conv_2d, max_pool_2d
from tflearn.layers.estimator import regression
from tflearn.data_preprocessing import ImagePreprocessing
from tflearn.data_augmentation import ImageAugmentation
from tflearn.data_utils import image_preloader

#import my network factory
from networkfactory import NetworkFactory

# Set up args
parser = argparse.ArgumentParser(description='Train a model using CNN on a folder of images')
parser.add_argument('--t', type=str, help='A directory container folders with images of classes to train', required=True)
parser.add_argument('--v', type=str, help='A directory container folders with images of classes to validate', required=True)
parser.add_argument('--x', type=int, help='pixel count in x dimension', required=True)
parser.add_argument('--y', type=int, help='pixel count in y dimension', required=True)
parser.add_argument('--d', type=str, help='directory to save model', required=True)
args = parser.parse_args()

dimension_x = args.x
dimension_y = args.y

x, y = image_preloader(args.t,
                       image_shape=(dimension_x, dimension_y),
                       mode='folder',
                       categorical_labels=True,
                       normalize=True)
X_test, Y_test = image_preloader(args.v,
                                 image_shape=(dimension_x, dimension_y),
                                 mode='folder',
                                 categorical_labels=True,
                                 normalize=True)
#TODO: we need more data/and to separate some test data out so we arn't
#verifying against the same data, which obviously is not ok to do
x,y = shuffle(x, y)
X_test, Y_test = shuffle(X_test, Y_test)

img_prep = ImagePreprocessing()
img_prep.add_featurewise_zero_center()
img_prep.add_featurewise_stdnorm()

img_aug = ImageAugmentation()
img_aug.add_random_flip_leftright()
img_aug.add_random_rotation(max_angle=25.)
img_aug.add_random_blur(sigma_max=3.)

fact = NetworkFactory()

#network = fact.conv_2d_2(dimension_x, dimension_y, img_prep, img_aug)
network = fact.frcnn(dimension_x, dimension_y, img_prep, img_aug)

model = tflearn.DNN(network, tensorboard_verbose=0,
                    checkpoint_path=args.d+'/flower_classifier.tfl.ckpt')

model.fit(x, y, n_epoch=5000, shuffle=True, validation_set=(X_test, Y_test),
          show_metric=True, batch_size=64,
          snapshot_epoch=True, run_id='flower-classifier')

model.save('flower-classifier.tfl')
print('network trained and save as flower-classifier.tfl')
