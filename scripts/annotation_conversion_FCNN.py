import sys
import os
import time

file = sys.argv[1]
file_num = 0

with open(file) as f:
    out = open('{}'.format(sys.argv[2]), 'w')
    out.write('class,filename,height,width,xmax,xmin,ymax,ymin\n')
    for line in f.readlines():
        file_num += 1
        contents = line.split(' ', 1)
        filename = contents[0].split('/')[1]

        img_data = contents[1].strip('\n').split(' ',1)
        img_count = img_data[0]

        if ((int)(img_count)) is 0:
            continue

        dims = img_data[1].split(' ')
        length = int(len(dims)/4)
        for i in range(length):
            out.write('flower,{},{},{},{},{},{},{}\n'.format(filename,
                                                      dims[2],
                                                      dims[3],
                                                      (int)(dims[0])+(int)(dims[2]),
                                                      dims[0],
                                                      (int)(dims[1])+(int)(dims[3]),
                                                      dims[1]))
            if i is not int((len(dims)/4)-1):
                dims = dims[4:]
            else:
                continue
