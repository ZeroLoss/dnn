import argparse
import tflearn
from tflearn.data_utils import shuffle
from tflearn.layers.core import input_data, dropout, fully_connected
from tflearn.layers.conv import conv_2d, max_pool_2d
from tflearn.layers.estimator import regression
from tflearn.data_preprocessing import ImagePreprocessing
from tflearn.data_augmentation import ImageAugmentation
from tflearn.data_utils import image_preloader
from tflearn.layers.normalization import local_response_normalization
from tflearn.layers.estimator import regression

class NetworkFactory():

    def conv(self, x, y, img_prep, img_aug):

        network = input_data(shape=[None, x, y, 3],
                             data_preprocessing=img_prep,
                             data_augmentation=img_aug)

        network = conv_2d(network, 32, 5, padding='same', activation='relu')

        network = max_pool_2d(network, 2, strides=2)

        network = conv_2d(network, 64, 5, activation='relu')

        network = conv_2d(network, 64, 3, activation='relu')

        network = max_pool_2d(network, 2)

        network = fully_connected(network, 512, activation='relu')

        network = dropout(network, 0.5)

        network = fully_connected(network ,2, activation='softmax')

        network = regression(network, optimizer='adam',
                             loss='categorical_crossentropy',
                             learning_rate=0.001)
        return network

    ###################################
    ##Working on another network, NYI##
    ###################################
    def conv_2d_2(self, x, y, img_prep, img_aug):
        convnet = input_data(shape=[None, x, y, 3], data_preprocessing=img_prep,
                             data_augmentation=img_aug, name='input')

        convnet = conv_2d(convnet, 32, 2, activation='relu')
        convnet = max_pool_2d(convnet, 2)

        convnet = conv_2d(convnet, 64, 2, activation='relu')
        convnet = max_pool_2d(convnet, 2)

        convnet = conv_2d(convnet, 32, 2, activation='relu')
        convnet = max_pool_2d(convnet, 2)

        convnet = conv_2d(convnet, 64, 2, activation='relu')
        convnet = max_pool_2d(convnet, 2)

        convnet = conv_2d(convnet, 32, 2, activation='relu')
        convnet = max_pool_2d(convnet, 2)

        convnet = conv_2d(convnet, 64, 2, activation='relu')
        convnet = max_pool_2d(convnet, 2)

        convnet = fully_connected(convnet, 1024, activation='relu')
        convnet = dropout(convnet, 0.8)

        convnet = fully_connected(convnet, 2, activation='softmax')
        convnet = regression(convnet, optimizer='adam', learning_rate=1e-3, loss='categorical_crossentropy', name='targets')

        return convnet


    def frcnn(self, x, y, img_prep, img_aug, restore=False):
            # Building 'AlexNet'
            network = input_data(shape=[None, x, y, 3])
            network = conv_2d(network, 96, 11, strides=4, activation='relu')
            network = max_pool_2d(network, 3, strides=2)
            network = local_response_normalization(network)
            network = conv_2d(network, 256, 5, activation='relu')
            network = max_pool_2d(network, 3, strides=2)
            network = local_response_normalization(network)
            network = conv_2d(network, 384, 3, activation='relu')
            network = conv_2d(network, 384, 3, activation='relu')
            network = conv_2d(network, 256, 3, activation='relu')
            network = max_pool_2d(network, 3, strides=2)
            network = local_response_normalization(network)
            network = fully_connected(network, 4096, activation='tanh')
            network = dropout(network, 0.5)
            network = fully_connected(network, 4096, activation='tanh')
            network = dropout(network, 0.5)
            network = fully_connected(network, 2, activation='softmax', restore=restore)
            network = regression(network, optimizer='momentum',
                                 loss='categorical_crossentropy',
                                 learning_rate=0.001)

            return network
