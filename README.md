1. Install tensorflow - assuming that it exists on a ML machine already - assuming your in a VENV also.

2. ```git clone https://github.com/tensorflow/models.git```

3. Download model from following link:

```http://download.tensorflow.org/models/object_detection/faster_rcnn_nas_coco_2018_01_28.tar.gz```


4. Extract it to working directory

5. Create following working directory structure, and move the appropriate files into it from CNN repo

+WORKING_DIR

  -data/

     -object-detection.pbtext

     -test.record

     -train.record

     -faster_rcnn_nas_coco.config (can modify batch size etc if need be)

  -models/  (Which should be the git clone ... from above)

  -faster_rcnn_nas_coco_2018_01_28/    (The model we downloaded, could and maybe will if we get working try others, may get better results.)

  -images/ (all our raw images)


6. cp the data/ and images/ folders to models/research/object-detection

7. Run the following command. Double check paths both here and in the faster_rcnn_nas_coco.config file, which is now located in models/research/object-detection/


```python object_detection/model_main.py --pipeline_config_path=../faster_rcnn_nas_coco.config --model-dir=../../faster_rcnn_nas_coco_2018_01_28/model.ckpt --num_train_steps=50000 --sample_1_of_n_eval_examples=1 --alsologtostderr```
